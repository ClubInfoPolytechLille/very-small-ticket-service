<?php
require_once __DIR__ . '/../vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable("/etc/vsts");
$dotenv->load();

require_once('./Querier.php');
require_once('./Version.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>VSTS Club Info</title>
    <link rel="stylesheet" href="style.css">
    <style>
        p{
            padding: 0.4em;
        }
    </style>
    <script src="script.js"></script>
</head>
<body>

<h1>
    Very Small Ticket Service
    <span>v<?php echo Version::get()?></span>
</h1>

<?php
require_once('./Querier.php');

//existence (and quality) of post args
if( !isset($_GET['id'])){
    echo "<h2>Requête invalide</h2>";
}
else {
    $ticketContent = Querier::getOneTicket($_GET['id']);

    echo "<h2>Ticket " . $_GET['id'] . "</h2>";
}
?>

<div class="main">
    <p><label>Nom :</label><?php echo $ticketContent['1']; ?></p>

    <p><label>Contact (mail ou pseudo Discord) :</label><?php echo $ticketContent['2']; ?></p>

    <p><label>Sévérité/Urgence :</label><?php echo $ticketContent['3']; ?></p>

    <p><label>Projet :</label><?php echo $ticketContent['4']; ?></p>

    <p><label>Projet+ :</label><?php echo $ticketContent['5']; ?></p>

    <p><label>Sujet :</label><?php echo $ticketContent['6']; ?></p>

    <p><label>Description :</label><?php echo $ticketContent['7']; ?></p>

    <p><label>Status :</label><?php echo $ticketContent['8']; ?></p>
</div>

</body>
</html>
