<?php


class Antispam
{
    //init le token (sur index.php) et on garde la date !
    public static function openForm(){
        session_start();

        $_SESSION["beginFormDate"] = time();
    }

    //on check token sur send.php
    public static function tooFastForm(){
        session_start();

        if(!isset($_SESSION["beginFormDate"]) || time() - $_SESSION["beginFormDate"] < 5){
            return true;
        }else{
            $_SESSION["beginFormDate"] = null;
            return false;
        }
    }

}