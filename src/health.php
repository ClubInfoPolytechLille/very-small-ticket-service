<?php
require_once('./Querier.php');
require_once('./HookService.php');
require_once('./Version.php');

echo "init v" . Version::get() . "</br>";

//test existence of vars
require_once __DIR__ . '/../vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable("/etc/vsts");
$dotenv->load();
echo "vars are good, db port = " . $_SERVER['PSQL_PORT'] . "</br>";

//test connexion to the DB
if(!Querier::isDBTableExist()){
    error_log("VSTS can't find the table !");
    http_response_code(500);
    exit(1);
}
echo "db is good</br>";

//return 200 in that case
echo "success!";