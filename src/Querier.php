<?php


class Querier
{
    private static $db;
    const TABLE_NAME = 'ticket';
    public static $lastId;

    public static function reconnect()
    {
        Querier::$db = pg_pconnect(
            "host=" . $_SERVER['PSQL_HOST'] .
            " port=" . $_SERVER['PSQL_PORT'] .
            " dbname=" . $_SERVER['PSQL_DBNAME'] .
            " user=" . $_SERVER['PSQL_USER'] .
            " password=" . $_SERVER['PSQL_PASSWORD']
        );
        if (Querier::$db == false) {
            error_log("VSTS failed to connect to the db !");
            http_response_code(500);
            exit(1);//TODO should throw error
        }
    }

    public static function createNewTicket()
    {
        Querier::reconnect();

        $res = pg_query_params(Querier::$db,
            "insert into " . Querier::TABLE_NAME . " (reporter, contact, urgency, project, projectPlus, subject, description, status) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING uid",
            array($_POST['name'], $_POST['contact'], $_POST['urgency'], $_POST['project'], $_POST['projectplus'], $_POST['subject'], $_POST['description'], 'open')
        );

        if ($res == false) {
            exit(1); //TODO
        }

        Querier::$lastId = pg_fetch_array($res)["0"];
    }

    public static function isDBTableExist(){
        Querier::reconnect();
        $res = pg_query(Querier::$db, "SELECT to_regclass('" . $_SERVER['PSQL_DBNAME'] . ".public. " . self::TABLE_NAME . "')");// . Querier::TABLE_NAME);
        if(pg_fetch_row($res)[0] == self::TABLE_NAME){
            return true;
        }
        return false;
    }

    private static function sanitizeId($id){
        if(preg_match("/^CIR-[0-9]+/", $id) != 1) {
            error_log("does not match the pattern DBGONLY");
            exit(1);
        }
        preg_match('!\d+!', $id, $id);
        return $id;
    }

    public static function getOneTicket($id)
    {
        $id = Querier::sanitizeId($id);
        if($id == false){
            error_log("id is not well formatted");
            exit(1);
        }

        Querier::reconnect();
        $res = pg_query_params(Querier::$db,
            "SELECT * FROM " . Querier::TABLE_NAME . " where uid=$1",
            $id
        );

        return pg_fetch_row($res);
    }

    public static function getAllTickets()
    {
        Querier::reconnect();
        $res = pg_query("SELECT * FROM " . Querier::TABLE_NAME); //TODO miss db ?

        return pg_fetch_all($res);
    }

    private static function safeStatusQuery($id, $lastStatus, $newStatus){
        $res = pg_query_params(Querier::$db,
            "UPDATE " . Querier::TABLE_NAME . " SET status = $2 WHERE uid = $1 AND status = $3 RETURNING status",
            array($id, $newStatus, $lastStatus));

        if($res == false){
            return "dbNull";
        }

        return pg_fetch_array($res)["0"];
    }

    public static function pushStatusTicket($id, $lastStatus)
    {
        Querier::reconnect();

        if($lastStatus=='open'){
            return Querier::safeStatusQuery($id, 'open', 'progress');
        }
        elseif ($lastStatus=='progress'){
            return Querier::safeStatusQuery($id, 'progress', 'closed');
        }
        elseif ($lastStatus=='closed'){
            return 'Already-closed';
        }
        return "NA";
    }
}