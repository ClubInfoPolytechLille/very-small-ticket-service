<?php
require_once('./Antispam.php');

$isTooFast = Antispam::tooFastForm();

require_once __DIR__ . '/../vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable("/etc/vsts");
$dotenv->load();

require_once('./Querier.php');
require_once('./HookService.php');
require_once('./Version.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>VSTS Club Info</title>
    <link rel="stylesheet" href="style.css">
    <script src="script.js"></script>
</head>
<body>

<h1>
    Very Small Ticket Service
    <span>v<?php echo Version::get()?></span>
</h1>


<?php
//cheating with bot
if($isTooFast){
    error_log("invalid request because it was too fast for sending");
    echo "<h2>Requête invalide</h2>";
}

//existence (and quality) of post args
else if( !isset($_POST['name']) || !isset($_POST['contact']) || !isset($_POST['urgency']) || !isset($_POST['project']) || !isset($_POST['projectplus']) || !isset($_POST['subject']) || !isset($_POST['description'])){
    error_log("invalid request because missing a POST arg");
    echo "<h2>Requête invalide</h2>";
}
else {
    //process
    Querier::createNewTicket();//TODO should pass args?

    echo "<h2>Création de ticket réussie</h2>";
    //uid ticket
    $ticketUid = "CIR-".Querier::$lastId;
    HookService::hookTicket($ticketUid);

    echo "<h2>Félicitations, tu participes à l'amélioration du club avec ce nouveau ticket : ".$ticketUid." .</h2>";
}
?>

</body>
</html>
