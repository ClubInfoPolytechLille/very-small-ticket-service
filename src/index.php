<?php
    require_once('./Antispam.php');
    Antispam::openForm();

    require_once __DIR__ . '/../vendor/autoload.php';
    $dotenv = Dotenv\Dotenv::createImmutable("/etc/vsts");
    $dotenv->load();

    require_once('./Version.php');
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>VSTS Club Info</title>
    <link rel="stylesheet" href="style.css">
    <script src="script.js"></script>
</head>
<body>

<h1>
    Very Small Ticket Service
    <span>v<?php echo Version::get()?></span>
</h1>
<h2>Ouvrir un ticket</h2>

<form action="send.php" method="post">
    <label for="name">Nom :</label><br>
    <input type="text" id="name" name="name" placeholder="nom" required><br>
    <label for="contact">Contact :</label><br>
    <input type="text" id="contact" name="contact" placeholder="mail ou pseudo discord"><br>
    <label for="urgency">Sévérité/Urgence :</label><br>
    <select id="urgency" name="urgency" required>
        <option value="critic">Dysfonctionement majeur</option>
        <option value="minor" selected>Bug mineur</option>
        <option value="improve">Amélioration</option>
    </select><br>
    <label for="project">Lieu :</label><br>
    <select id="project" name="project" required>
        <option disabled selected value="">**choisir un projet**</option>
        <?php
        foreach(explode(",", $_SERVER["PROJECT_LIST"]) as $item) {
            echo '<option value="' . $item . '">' . $item . '</option>';
        }
        ?>
    </select>
    <input type="text" id="projectplus" name="projectplus" placeholder="précisions (optionnel)"><br>
    <label for="subject">Sujet :</label><br>
    <input type="text" id="subject" name="subject" placeholder="en peu de mots" required><br>
    <label for="description">Description :</label><br>
    <textarea type="text" id="description" name="description" cols="40" rows="5" required></textarea><br>

    <input type="submit" class="btn-item" value="Envoyer">
</form>

<script>
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const paramPrefillNames = ["name", "contact", "urgency", "project", "projectplus", "subject"]


    function prefillIfExist(paramName){
        if(!urlParams.has(paramName)){
            return;
        }

        let prefillValue = urlParams.get(paramName);

        let strict = false;
        if(prefillValue.startsWith("!")){
            prefillValue = prefillValue.substring(1);
            strict = true;
        }

        document.getElementById(paramName).value = prefillValue;
        document.getElementById(paramName).disabled = strict;
    }

    for(const paramName of paramPrefillNames){
        prefillIfExist(paramName)
    }


    function reEnableAllTheForm(){
        for(const paramName of paramPrefillNames){
            document.getElementById(paramName).disabled = false;
        }
    }

    document.querySelector("form").addEventListener("submit", reEnableAllTheForm);
</script>

</body>
</html>
