<?php

require_once('./Querier.php');
require_once __DIR__ . '/../vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable("/etc/vsts");
$dotenv->load();
require_once('./Version.php');


if (!isset($_SERVER['PHP_AUTH_USER'])) {
    // If no username provided, present the auth challenge.
    header('WWW-Authenticate: Basic realm="My Website"');
    header('HTTP/1.0 401 Unauthorized');
    // User will be presented with the username/password prompt
    // If they hit cancel, they will see this access denied message.
    echo '<p>Access denied. You did not enter a password.</p>';
    exit; // Be safe and ensure no other content is returned.
}

if ($_SERVER['DBG_PASSTHROUGH'] == 'true'
    || $_SERVER['PHP_AUTH_USER'] == $_SERVER['ADMIN_USERNAME'] && $_SERVER['PHP_AUTH_PW'] == $_SERVER['ADMIN_PASSWORD']
) {

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>VSTS Club Info</title>
    <link rel="stylesheet" href="style.css">
    <script src="script.js"></script>
</head>
<body>

<h1>
    VSTS admin
    <span>v<?php echo Version::get()?></span>
</h1>

<strong>
    <?php
        if(isset($_POST['uid']) && isset($_POST['lastStatus'])){
            $newStatus = Querier::pushStatusTicket($_POST['uid'], $_POST['lastStatus']);
            echo "La carte CIR-" . $_POST['uid'] . " est mise à jour en '" . $newStatus . "' maintenant !";
        }
        else{
            echo "Connecté !";
        }
    ?>
</strong>

<h2>Statistiques</h2>
Soon
<h2>Liste des tickets (brute)</h2>

<?php
$tickets = Querier::getAllTickets();
?>

<table id="employee_grid" class="table" width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>id</th>
        <th>name</th>
        <th>contact</th>
        <th>urgency</th>
        <th>project</th>
        <th>projectplus</th>
        <th>subject</th>
        <th>description</th>
        <th>status</th>
        <th>action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($tickets as $key => $ticket) :?>
        <tr>
            <td>CIR-<?php echo $ticket['uid'] ?></td>
            <td><?php echo $ticket['reporter'] ?></td>
            <td><?php echo $ticket['contact'] ?></td>
            <td><?php echo $ticket['urgency'] ?></td>
            <td><?php echo $ticket['project'] ?></td>
            <td><?php echo $ticket['projectplus'] ?></td>
            <td><?php echo $ticket['subject'] ?></td>
            <td><?php echo $ticket['description'] ?></td>
            <td><?php echo $ticket['status'] ?></td>
            <td>
                <form method="POST" action="#">
                    <input type="hidden" name="uid" value="<?php echo $ticket['uid'] ?>"/>
                    <input type="hidden" name="lastStatus" value="<?php echo $ticket['status'] ?>"/>
                    <?php
                        if ($ticket['status'] == 'open') {
                            echo '<input type="submit" class="no-margin btn-item" value="Commencer"/>';
                        }
                        elseif ($ticket['status'] == 'progress'){
                            echo '<input type="submit" class="no-margin btn-item" value="Fermer"/>';
                        }
                    ?>
                </form>

            </td>
        </tr>
    <?php endforeach;?>
    </tbody>
</table>





<?php

} else {
    header('WWW-Authenticate: Basic realm="My Website"');
    header('HTTP/1.0 401 Unauthorized');
    echo '<p>Access denied! You do not know the password.</p>';
}