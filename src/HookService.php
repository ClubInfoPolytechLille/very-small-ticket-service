<?php


class HookService
{


    private static function curlPost($url, $data = NULL, $headers = []) {
        $ch = curl_init($_SERVER['DISCORD_URL']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5); //timeout in seconds
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_ENCODING, 'identity');


        if (!empty($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        $response = curl_exec($ch);
        if (curl_error($ch)) {
            trigger_error('Curl Error:' . curl_error($ch));
        }

        curl_close($ch);
        return $response;
    }

    public static function hookHello(){
        HookService::curlPost("https://discord.com/api/webhooks/850724592916889620/8kCM5K1y1xVvB5ijaI2YtGi2MBeQGiLINEw8zY49ydulnweEg1L6D9-eBxwTR3UpNMIs", ["content"=>"Hey there, I'm a happy bot", "username"=>"VSTS-TEST"]);
    }

    public static function hookTicket($id){
        HookService::curlPost("https://discord.com/api/webhooks/850724592916889620/8kCM5K1y1xVvB5ijaI2YtGi2MBeQGiLINEw8zY49ydulnweEg1L6D9-eBxwTR3UpNMIs", ["content"=>"Woops, someone has raised a new issue, its name is [$id](" . $_SERVER['HOST_URL'] . "display.php?id=$id)", "username"=>"VSTS-TEST"]);
    }
}