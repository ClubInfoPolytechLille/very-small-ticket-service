DROP TABLE if exists ticket;
DROP TYPE if exists urgencyEnum;
DROP TYPE if exists statusEnum;

CREATE TYPE urgencyEnum AS ENUM ('critic', 'minor', 'improve');
CREATE TYPE statusEnum AS ENUM ('open', 'progress', 'closed');

CREATE TABLE ticket
(
uid SERIAL PRIMARY KEY,
reporter varchar(20),
contact varchar(20), /*please put the mail/discord table*/
urgency urgencyEnum,
project varchar(20), /*please be relational*/
projectPlus varchar(50),
subject varchar(50),
description varchar(500),
status statusEnum
);

insert into ticket VALUES (DEFAULT, 'root', 'touch "/"', 'improve', 'vsts', 'création de la db', 'juste un example', 'besoin d un example pour essayer la bdd', 'closed');