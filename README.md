# Very Small Ticket Service

Service permettant de créer un ticket pour un bug ou une amélioration (si provient d'un utilisateur final et qu'il ne connaît pas les dev du projet en question).

## Utilisation

La création de ticket est accessible sans connexion.

L'affichage d'un ticket par son id est disponible par tous, par exemple sur <http://club.plil.fr/vsts/display.php?id=CIR-1> .

Si vous êtes dans la gestion d'un tiers-projet et que vous voulez rediriger vos utilisateurs, vous pouvez injecter des valeurs pour des champs. Si vous voulez forcer la valeur il faut ajouter un point d'exclamation '!' au début, comme avec le contact ou le projet dans l'exemple suivant :

```
http://club.plil.fr/vsts/?name=Axel&!contact=axel@gmail.com&urgency=critic&project=!gcr&projectplus=sauvegarde%20odj&subject=sauvegarde%20bloqu%C3%A9e
```

Urgency peut prendre les valeurs "critic"/"minor"/"improve" .

Soyez sûr que votre projet est bien configuré pour VSTS.

Il existe aussi une interface administrateur pour afficher tous les tickets et les manipuler.

## Déploiement

Il faut mettre le fichier .env dans /etc/vsts

Il faut une db PSQL paramétré comme dans le .env avec au moins 1 ligne.

Vous pouvez vérifier l'état du service en observant un code http 200 ET la bonne sortie sur /health.php

Pour le conteneur, on copie le dossier vendor du dev, c'est un peu sale (à changer rapidement), en plus, vendor est dans le git (pour le CI)...
